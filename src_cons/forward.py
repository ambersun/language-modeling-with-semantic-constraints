import numpy as np
D1 = np.array([[1,3],[2,4]])
D2 = np.array([[4,2],[3,1]])
D3 = np.array([[1,5],[3,7]])
M = np.array([[1,3,5,7,9],[2,4,6,8,10]])
w1 = np.array([[1],[0],[0],[0],[0]])
w2 = np.array([[0],[1],[0],[0],[0]])
w3 = np.array([[0],[0],[1],[0],[0]])
w4 = np.array([[0],[0],[0],[1],[0]])

A1 = np.dot(D1,np.dot(M,w1))
A2 = np.dot(D2,np.dot(M,w2))
A3 = np.dot(D3,np.dot(M,w3))

S = np.dot(M.T,A1+A2+A3)

print S
