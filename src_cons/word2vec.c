/* word2vec.c */

#include <stdio.h>
#include <stdlib.h>
#include "util.c"
#include "constraints.c"
#include <string.h> 
#include "check_model.c"

void forward_pass(struct model * m, struct parameters * p, Mat * y, int * wid)
{
	// y = M^T(\sum_i D_i*M*w_i)+b, where i is from 0 to n-2, wid[n-1] is the target
	
	int i;
	Mat * v = init_mat(p->dim,1,ZEROS);
	Mat * Dv = init_mat(p->dim,1,ZEROS);

	for(i=0;i<p->ngram-1;i++)
	{
		// v = v_i = M*w_i, select a column from M
		col_select(v,m->M,wid[i]);
		// Dv += D_i*v_i
		// mat_mult(Mat * C, double alpha, Mat * A, trans_opts transA, Mat * B, trans_opts transB, double beta )
		mat_mult(Dv,1.0,m->D[i],notrans,v,notrans,1.0);
	}
	// y = b	
	col_select(y,m->b,0);

	// y = M^T * Dv + y
	mat_mult(y,1.0,m->M,trans,Dv,notrans,1.0);
	
	// y = softmax(y)
	softmax(y,y);
	free_mat(v);
	free_mat(Dv);
}

void backward_pass(struct model * m, struct parameters * p, Mat * y, int * wid, struct grad * g)
{
	// softmax + cross entropy loss
	int i;
	Mat * MxErr = init_mat(p->dim,1,ZEROS);  // M*(y - t)
	Mat * v = init_mat(p->dim,1,ZEROS);  // v_i = M*w_i
	Mat * Dv = init_mat(p->dim,1,ZEROS);  // temp variable
	Mat * t = init_mat(p->dict_size,1,ZEROS); //temp variable
	
	t->value[wid[p->ngram-1]] = 1.0;
	// dEdb = y - t
	elem_op(g->dEdb,1.0,y,1.0,t,MINUS);
	
	// dEdD[i] = M * dEdb * (M * w_i)^T = M * dEdb * v_i^T = MxErr * v_i^T
	// dEdM = \sum_i D[i]^T*M*(y-t)*t^T + D[i]*M*t*(y-t)^T = \sum_i D[i]^T*MxErr*t^T + D[i]*v_t*dEdb^T
	mat_mult(MxErr,1.0,m->M,notrans,g->dEdb,notrans,0.0);
	t->value[wid[p->ngram-1]] = 0.0;

	for(i=0;i<p->ngram-1;i++)
	{
		// v = v_i 	
		col_select(v,m->M,wid[i]);
		// dEdD[i] = MxErr * v 
		mat_mult(g->dEdD[i],1.0,MxErr,notrans,v,trans,0.0); 
	
		// Dv = D[i]^T*MxErr
		mat_mult(Dv,1.0,m->D[i],trans,MxErr,notrans,0.0);

 		t->value[wid[i]] = 1.0;  // t = w_i
		// dEdM = Dv*t^T
		mat_mult(g->dEdM,1.0,Dv,notrans,t,trans,1.0);
		
		// Dv = D[i]*v_t
		mat_mult(Dv,1.0,m->D[i],notrans,v,notrans,0.0); 

		// dEdM += Dv*dEdb^T
		mat_mult(g->dEdM,1.0,Dv,notrans,g->dEdb,trans,1.0);
 
		t->value[wid[i]] = 0.0;
	}
	
	free_mat(MxErr);
	free_mat(v);
	free_mat(t);
	free_mat(Dv);
}

void update_model(struct model * m, struct parameters * p, struct grad * g)
{
	elem_op(m->b,1.0,m->b,p->lr,g->dEdb,MINUS);
	elem_op(m->M,1.0,m->M,p->lr,g->dEdM,MINUS);
	int i;
	for(i=0;i<p->ngram-1;i++)
	 	elem_op(m->D[i],1.0,m->D[i],p->lr,g->dEdD[i],MINUS);
}

void test_model(struct model * m, struct parameters * p, char * mode, constr ** c)
{
	char * line = NULL;
	char *saveptr;
        size_t len = 0;
	char * pch;
	Mat * y = init_mat(p->dict_size,1,ZEROS);
	int * wid = (int *)malloc(p->ngram * sizeof(int)); 
	int i;
	

	double cnt = 0.0;
	double ppl = 0.0;
	double etp = 0.0;
	FILE * f;

	double rnd;

	if(strcmp(mode,"train") == 0)	f = fopen(p->train_file,"r");
	else if(strcmp(mode,"valid") == 0)	f = fopen(p->valid_file,"r");
	else if(strcmp(mode,"test") == 0)	f = fopen(p->test_file,"r");
	else	return;
	
	while ((getline(&line, &len, f)) != -1) { 
		
		if(strcmp(mode,"train") == 0)
		{
			rnd = (double)rand()/(double)RAND_MAX;
			if(rnd > 0.05)
				continue;
		}

		for(i=0;i<p->ngram;i++)	wid[i] = -1;
		pch = strtok_r (line," ",&saveptr);
  		while (pch != NULL)
  		{	
			// left shift				
			for(i=0;i<p->ngram-1;i++)
			{
				wid[i] = wid[i+1];
			}
															
			wid[p->ngram-1] = atoi(pch);		
    			pch = strtok_r (NULL, " ",&saveptr);

			if(wid[0]!=-1)
			{
				struct grad * g = init_grad(p);
				forward_pass(m,p,y,wid);
				cnt += 1.0;
				etp += log(y->value[wid[p->ngram-1]])/log(2);
			}							
  		}
		break;	
	}
	
	ppl = powf(2.0,-etp/cnt);

	printf("Perplexity: %f\n",ppl);

	if(strcmp(mode,"train") == 0) m->train_ppl = ppl;
	if(strcmp(mode,"valid") == 0) m->valid_ppl = ppl;
	if(strcmp(mode,"test") == 0) m->test_ppl = ppl;

	fclose(f);
	if(line) free(line);

	test_cons(c, m, p, mode);

	free(wid);
	free_mat(y);
}

void train_model ( struct parameters * p )
{ 
	struct model * m = init_model(p);

	int epoch,i;
	char * line = NULL;
	char *saveptr;
        size_t len = 0;
	char * pch;
	Mat * y = init_mat(p->dict_size,1,ZEROS);
	int * wid = (int *)malloc(p->ngram * sizeof(int)); 

	int cnt=0;

	double pcnt = 0.0;
	double ppl = 0.0;
	double etp = 0.0;
	double prob;

	constr ** train_constr = (constr **)malloc(p->categories*sizeof(constr *));
	constr ** valid_constr = (constr **)malloc(p->categories*sizeof(constr *));
	constr ** test_constr = (constr **)malloc(p->categories*sizeof(constr *));

	load_cons(train_constr,valid_constr,test_constr,p);

	printf("Start training.\n");
	
	for(epoch = 1; epoch <= p->epoch; epoch++)
	{	
		
		FILE * ftrain = fopen(p->train_file,"r");

		printf("Number of epoch: %d\n",epoch);
		while ((getline(&line, &len, ftrain)) != -1) {  // printf("%d\n",cnt++);
				
			if(pcnt >= 1000)
				break;
			printf("%d\n",(int)pcnt);
			p->epoch = 1;

			for(i=0;i<p->ngram;i++)
			{
				wid[i] = -1;
			}

  			pch = strtok_r (line," ",&saveptr);
  			while (pch != NULL)
  			{	
				// left shift				
				for(i=0;i<p->ngram-1;i++)
				{
					wid[i] = wid[i+1];
				}
																
				wid[p->ngram-1] = atoi(pch);		
    				pch = strtok_r (NULL, " ",&saveptr);

				// train model
				if(wid[0]!=-1)
				{
					struct grad * g = init_grad(p);
					
					forward_pass(m,p,y,wid);
					backward_pass(m,p,y,wid,g);
					norm_cons(m,p);					

					update_model(m,p,g);
					
					free_grad(g,p);

					pcnt += 1.0;
					// etp += log(y->value[wid[p->ngram-1]])/log(2);
				}	
				// break;
  			}

			prob = (double)rand()/(double)RAND_MAX;
			if(prob<0.02*epoch)
			{
				struct grad * g_sem = init_grad(p);	
				sem_cons(m,p,g_sem,train_constr);
				update_model(m,p,g_sem);
				free_grad(g_sem,p);
			}
		
			// ppl = powf(2.0,-etp/pcnt);
			// printf("%f\n",ppl);
		}
		fclose(ftrain);
		printf("=====Testing model on training set=====\n");
		test_model(m,p,"train",train_constr);
		printf("=====Testing model on validation set=====\n");
		test_model(m,p,"valid",valid_constr);
		printf("=====Testing model on test set=====\n");
		test_model(m,p,"test",test_constr);

		printf("Train accuracy: %f\n",m->train_accu);
		printf("Valid accuracy: %f\n",m->valid_accu);
		printf("Test accuracy: %f\n",m->test_accu);
	}	
	
	if(line)
		free(line);
	free(wid);
	free_model(m,p);
	free_mat(y);
	free_constr(train_constr,p);
	free_constr(valid_constr,p);
	free_constr(test_constr,p);
}

int main ( )
{
	struct parameters * p = load_parameters();	
	train_model(p);
	free(p->train_file);
	free(p->valid_file);
	free(p->test_file);
	free(p);

	return 1;
}
