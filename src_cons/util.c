#include <stdio.h>
#include <stdlib.h>
#include "cblas.h"
#include <string.h> 
#include <math.h>
#include "mat.c"


struct parameters
{
	int dim; // embedding dimension
	int dict_size; // dictionary size
	int epoch; // number of iterations for training
	int ngram; // number of grams
	double lr; // learning rate
	int categories; // number of categories in constraints
	double lambda_norm; // lagrange multiplier for norm constraints
	double lambda_sem;  // lagrange multiplier for sematic constraints
	double margin;
  
	char * train_file;
	char * valid_file;
	char * test_file;
	char * constraints;
	
};

struct model
{
	Mat * M; // embedding matrix, dim*dict_size
	Mat * b; // bias  dict_size*1
	Mat ** D; // weights  dim*dim
	double train_ppl;
	double valid_ppl;
	double test_ppl;
	double train_accu;
	double valid_accu;
	double test_accu;
};

struct grad
{
	Mat * dEdM; // gradient to embedding matrix
	Mat * dEdb; // gradient to bias
	Mat ** dEdD; // gradient to weights
};

struct parameters * load_parameters ()
{
	struct parameters * p = (struct parameters *)malloc(sizeof(struct parameters));

	FILE * fp;
        char * line = NULL;
	char * pch = NULL;
        size_t len = 0;

        fp = fopen("config.txt", "r");

        while ((getline(&line, &len, fp)) != -1) {
		if(strstr(line,"embedding dimension") != NULL)
		{
			pch = strchr(line,':');
			p->dim = atoi(pch+2);
		}
		else if(strstr(line,"dictionary length") != NULL)
		{
			pch = strchr(line,':');
			p->dict_size = atoi(pch+2);
		}
		else if(strstr(line,"number of grams") != NULL)
		{
			pch = strchr(line,':');
			p->ngram = atoi(pch+2);
		}
		else if(strstr(line,"number of epochs") != NULL)
		{
			pch = strchr(line,':');
			p->epoch = atoi(pch+2);
		}
		else if(strstr(line,"learning rate") != NULL)
		{
			pch = strchr(line,':');
			p->lr = atof(pch+2);
		}
		else if(strstr(line,"norm lambda") != NULL)
		{
			pch = strchr(line,':');
			p->lambda_norm = atof(pch+2);
		}
		else if(strstr(line,"semantic lambda") != NULL)
		{
			pch = strchr(line,':');
			p->lambda_sem = atof(pch+2);
		}
		else if(strstr(line,"margin") != NULL)
		{
			pch = strchr(line,':');
			p->margin = atof(pch+2);
		}
		else if(strstr(line,"categories") != NULL)
		{
			pch = strchr(line,':');
			p->categories = atof(pch+2);
		}
		else if(strstr(line,"train corpus") != NULL)
		{
			pch = strchr(line,':');
			* (pch+strlen(pch)-1) = '\0';
			pch += 2;
			p -> train_file = (char*)malloc((strlen(pch)+1)*sizeof(char));
			strcpy(p->train_file, pch);					
		}
		else if(strstr(line,"valid corpus") != NULL)
		{
			pch = strchr(line,':');
			* (pch+strlen(pch)-1) = '\0';
			pch += 2;
			p -> valid_file = (char*)malloc((strlen(pch)+1)*sizeof(char));
			strcpy(p->valid_file, pch);
		}
		else if(strstr(line,"test corpus") != NULL)
		{
			pch = strchr(line,':');
			* (pch+strlen(pch)-1) = '\0';
			pch += 2;
			p -> test_file = (char*)malloc((strlen(pch)+1)*sizeof(char));
			strcpy(p->test_file, pch);			
		}
		else if(strstr(line,"constraints") != NULL)
		{
			pch = strchr(line,':');
			* (pch+strlen(pch)-1) = '\0';
			pch += 2;
			p -> constraints = (char*)malloc((strlen(pch)+1)*sizeof(char));
			strcpy(p->constraints, pch);			
		}
        }

        fclose(fp);
        if (line)
		free(line);
	return p; 
}

struct model * init_model(struct parameters * p)
{
	struct model * m = (struct model *)malloc(sizeof(struct model));
	m->M = init_mat(p->dim,p->dict_size,RAND);
	m->b = init_mat(p->dict_size,1,RAND);
	m->D = (Mat **)malloc((p->ngram-1)*sizeof(Mat *));
	int i;
	for(i=0;i<p->ngram-1;i++)
	{
		m->D[i]=init_mat(p->dim,p->dim,RAND);
	}

	m->train_ppl = 0.0;
	m->valid_ppl = 0.0;
	m->test_ppl = 0.0;
	m->train_accu = 0.0;
	m->valid_accu = 0.0;
	m->test_accu = 0.0;
	
	return m;
} 

void free_model(struct model * m, struct parameters * p)
{
	free_mat(m->M);
	free_mat(m->b);
	int i;
	for(i=0;i<p->ngram-1;i++)
	{
		free_mat(m->D[i]);
	}
	free(m->D);
	free(m);
}

struct grad * init_grad(struct parameters * p)
{
	struct grad * g = (struct grad *)malloc(sizeof(struct grad));
	g->dEdM = init_mat(p->dim,p->dict_size,ZEROS);
	g->dEdb = init_mat(p->dict_size,1,ZEROS);
	g->dEdD = (Mat **)malloc((p->ngram-1)*sizeof(Mat *));
	int i;
	for(i=0;i<p->ngram-1;i++)
	{
		g->dEdD[i] = init_mat(p->dim,p->dim,ZEROS);
	}

	return g;
}

void free_grad(struct grad * g, struct parameters * p)
{
	int i;
	for(i=0;i<p->ngram-1;i++)
	{
		free_mat(g->dEdD[i]); 
	}
	free_mat(g->dEdM);
	free_mat(g->dEdb);
	free(g->dEdD);
}
