#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <math.h>
#include <cblas.h>

typedef enum{ZEROS,ONES,RAND} init_opts;
typedef enum{PLUS,MINUS,TIMES,DIVIDE} opts;
typedef enum{trans,notrans} trans_opts;
typedef enum{false,true} bool; 

typedef struct mat
{
	double * value; // values are stored column wise
	int rows;
	int cols;
}Mat;

Mat * init_mat(int rows,int cols,init_opts opts)
{
	Mat * matrix = (Mat *)malloc(sizeof(Mat));
	matrix->rows = rows;
	matrix->cols = cols;
	matrix->value = (double *)malloc(rows*cols*sizeof(double));

	int i;
	if(opts == ZEROS)
	{
		for(i=0;i<rows*cols;i++)
		{
			matrix->value[i]=0; 
		} 
	}
	else if (opts == ONES)
	{
		for(i=0;i<rows*cols;i++)
		{
			matrix->value[i]=1; 
		} 
	}
	else if (opts == RAND)
	{
		srand(0); // initialize vectors and matrices
		for(i=0;i<rows*cols;i++)
		{
			matrix->value[i]= (double)rand()/(double)RAND_MAX/20.0; 
		} 
	}
	
	return matrix;
}

void free_mat(Mat * matrix)
{
	free(matrix->value);
	free(matrix);
}

void col_select(Mat * dest, Mat * src, int col)
{
	memcpy(dest->value, src->value + col*src->rows, src->rows*sizeof(double));
}


void elem_op(Mat * dest, double alpha, Mat * src1, double beta, Mat * src2, opts op)
{
	if(src1->rows != src2->rows || src1->cols != src2->cols)
	{
		printf("error: different dimensions for elem_op.\n");
		return;
	}
	int i;
	if(op == PLUS)
		for(i=0;i<src1->rows*src1->cols;i++)
			dest->value[i] = alpha*src1->value[i]+beta*src2->value[i];

	else if(op == MINUS)
		for(i=0;i<src1->rows*src1->cols;i++)
			dest->value[i] = alpha*src1->value[i]-beta*src2->value[i];

	else if(op == TIMES)
		for(i=0;i<src1->rows*src1->cols;i++)
			dest->value[i] = alpha*src1->value[i]*beta*src2->value[i];

	else if(op == DIVIDE)
		for(i=0;i<src1->rows*src1->cols;i++)
			dest->value[i] = alpha*src1->value[i]/beta/src2->value[i];
}

void mat_mult(Mat * C, double alpha, Mat * A, trans_opts transA, Mat * B, trans_opts transB, double beta )
{	
	// C = alpha*A*B + beta*C
	int rows,cols,inner,indA,indB,i,j,k;
	bool errflag = false;
	enum CBLAS_TRANSPOSE transa, transb;

	if(transA == notrans) {rows = A->rows;inner = A->cols;transa=CblasNoTrans;}
	else {rows = A->cols; inner = A->rows;transa=CblasTrans;}
	if(transB == notrans)
	{ 
		cols = B->cols;
		transb=CblasNoTrans;
		if(inner!=B->rows) errflag = true;
	}
	else
	{ 
		cols = B->rows;
		transb=CblasTrans;
		if(inner!=B->cols) errflag = true;
	}
	if(C->rows != rows || C->cols != cols)
		errflag = true;

	if(errflag == true)
	{
		printf("error: dimension mismatch for mat_mult.\n");
		return;
	}

	// matrices are stored in column-wise manner
	if(cols == 1 && B->cols == 1)
		cblas_dgemv(CblasColMajor,transa,A->rows,A->cols,alpha,A->value,A->rows,B->value,1.0,beta,C->value,1.0);
	else if(inner == 1 && A->cols == 1 && B->rows == 1 && beta == 1.0)
		cblas_dger(CblasColMajor,rows,cols,alpha,A->value,1,B->value,1,C->value,rows);
	else
		cblas_dgemm(CblasColMajor,transa,transb,rows,cols,inner,alpha,A->value,A->rows,B->value,B->rows,beta,C->value,rows);
}

void softmax(Mat * dest, Mat * src)
{
	
	if(src->rows != 1 && src->cols != 1)
	{
		printf("error: non-vector for softmax.\n");
		return;
	}

	int vdim;
	if(src->rows > 1) vdim = src->rows;
	else vdim = src->cols;	
	
	// find max value in the vector
	double ymax = src->value[0], ysum = 0.0;
	int i;
	for(i=0;i<vdim;i++)
	{
		if(ymax < src->value[i])
			ymax = src->value[i];
	}
	// softmax function
	for(i=0;i<vdim;i++)
	{
		dest->value[i]=exp(src->value[i]-ymax);
		ysum += dest->value[i];
	}
	for(i=0;i<vdim;i++)
	{
		dest->value[i]=dest->value[i]/ysum;
	}	
}
