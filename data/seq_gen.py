import numpy as np
import pickle
import sys

if __name__ == "__main__":
    	path = sys.argv[-1]

with open('./' + path + '/vec.txt') as f:
	vectors = f.readlines();
	pairs = []	

	for i in xrange(1,len(vectors)):
		vector = vectors[i].rstrip().split()
		pairs.append((vector[0],np.array([float(x) for x in vector[1:]])))
	
	dictionary = dict(pairs)
	
with open('./'+path+'/wordlist.txt','w+') as f:
	keys = dictionary.keys()
	for word in sorted(keys):
		f.write(word+'\n')
	
pickle.dump(dictionary, open('./'+path+'/dictionary.pickle', 'w+'))


