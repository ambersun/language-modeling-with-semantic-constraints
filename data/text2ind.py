import sys

def convert(doc,wlist):
	with open(wlist) as f:
		wordlist = f.readlines()

	with open(doc) as f:
		lines = f.readlines()

	fname = doc[0:-4]+'.ind.txt'
	with open(fname,'w+') as f:	
		for line in lines:
			toks = line.rstrip().split(' ')
			for t in toks:
				if t!='' and t+'\n' not in wordlist:
					t = '<unk>'			

			f.write(' '.join(str(wordlist.index(t+'\n')) for t in toks if t!='')+'\n')



if __name__ == '__main__':
	if len(sys.argv) != 3:
		print 'invalid call'
		print 'try: python text2ind.py ./ptb/ptb.train.txt ./ptb/wordlist.txt'
		sys.exit()
	doc = sys.argv[1]
	wlist = sys.argv[2]
	convert(doc,wlist)



