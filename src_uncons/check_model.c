#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <math.h>

void check_mat()
{
	Mat * a = init_mat(2,3,ZEROS);
	a->value[0] = 1.0;
	a->value[1] = 2.0;
	a->value[2] = 3.0;
	a->value[3] = 4.0;
	a->value[4] = 5.0;
	a->value[5] = 6.0;
	
	Mat * b = init_mat(3,2,ZEROS);
	b->value[0] = 6.0;
	b->value[1] = 5.0;
	b->value[2] = 4.0;
	b->value[3] = 3.0;
	b->value[4] = 2.0;
	b->value[5] = 1.0;

	Mat * c = init_mat(2,2,ONES);

	mat_mult(c,1.0,a,notrans,b,notrans,0.0);
	int i;
	for(i=0;i<c->rows*c->cols;i++)
		printf("%f\n",c->value[i]);
	free_mat(a);
	free_mat(b);
	free_mat(c);
	return;
}

void check_softmax()
{
	Mat * a = init_mat(6,1,ZEROS);
	a->value[0] = 1.0;
	a->value[1] = 2.0;
	a->value[2] = 3.0;
	a->value[3] = 4.0;
	a->value[4] = 5.0;
	a->value[5] = 6.0;
	
	Mat * c = init_mat(6,1,ZEROS);

	softmax(c,a);
	int i;
	for(i=0;i<c->rows*c->cols;i++)
		printf("%f\n",c->value[i]);

	return;
}

void check_col_select()
{
	Mat * a = init_mat(3,2,ZEROS);
	a->value[0] = 1.0;
	a->value[1] = 2.0;
	a->value[2] = 3.0;
	a->value[3] = 4.0;
	a->value[4] = 5.0;
	a->value[5] = 6.0; 
	
	Mat * c = init_mat(3,1,ZEROS);
	col_select(c,a,1);
	int i;
	printf("%d,%d\n",c->rows,c->cols);
	for(i=0;i<c->rows*c->cols;i++)
		printf("%f\n",c->value[i]);

	return;
}

void fake_forward(struct model * m, struct parameters * p, Mat * y, int * wid)
{
	int i;

	Mat * v = init_mat(p->dim,1,ZEROS);
	Mat * Dv = init_mat(p->dim,1,ZEROS);

	for(i=0;i<p->ngram-1;i++)
	{
		// v = v_i = M*w_i, select a column from M
		col_select(v,m->M,wid[i]);
		// Dv += D_i*v_i
		// mat_mult(Mat * C, double alpha, Mat * A, trans_opts transA, Mat * B, trans_opts transB, double beta )
		mat_mult(Dv,1.0,m->D[i],notrans,v,notrans,1.0);
	}
	// y = b	
	col_select(y,m->b,0);

	// y = M^T * Dv + y
	mat_mult(y,1.0,m->M,trans,Dv,notrans,1.0);
	
	// y = softmax(y)
	softmax(y,y);
	free_mat(v);
	free_mat(Dv);
}

void fake_backward(struct model * m, struct parameters * p, Mat * y, int * wid, struct grad * g)
{
	// softmax + cross entropy loss
	int i;
	Mat * MxErr = init_mat(p->dim,1,ZEROS);  // M*(y - t)
	Mat * v = init_mat(p->dim,1,ZEROS);  // v_i = M*w_i
	Mat * Dv = init_mat(p->dim,1,ZEROS);  // temp variable
	Mat * t = init_mat(p->dict_size,1,ZEROS); //temp variable
	
	t->value[wid[p->ngram-1]] = 1.0;
	// dEdb = y - t
	elem_op(g->dEdb,1.0,y,1.0,t,MINUS);
	
	// dEdD[i] = M * dEdb * (M * w_i)^T = M * dEdb * v_i^T = MxErr * v_i^T
	// dEdM = \sum_i D[i]^T*M*(y-t)*t^T + D[i]*M*t*(y-t)^T = \sum_i D[i]^T*MxErr*t^T + D[i]*v_t*dEdb^T
	mat_mult(MxErr,1.0,m->M,notrans,g->dEdb,notrans,0.0);
	t->value[wid[p->ngram-1]] = 0.0;

	for(i=0;i<p->ngram-1;i++)
	{
		// v = v_i 	
		col_select(v,m->M,wid[i]);
		// dEdD[i] = MxErr * v 
		mat_mult(g->dEdD[i],1.0,MxErr,notrans,v,trans,0.0); 
	
		// Dv = D[i]^T*MxErr
		mat_mult(Dv,1.0,m->D[i],trans,MxErr,notrans,0.0);

 		t->value[wid[i]] = 1.0;  // t = w_i
		// dEdM = Dv*t^T
		mat_mult(g->dEdM,1.0,Dv,notrans,t,trans,1.0);
		
		// Dv = D[i]*v_t
		mat_mult(Dv,1.0,m->D[i],notrans,v,notrans,0.0); 

		// dEdM += Dv*dEdb^T
		mat_mult(g->dEdM,1.0,Dv,notrans,g->dEdb,trans,1.0);
 
		t->value[wid[i]] = 0.0;
	}
	
	free_mat(MxErr);
	free_mat(v);
	free_mat(t);
	free_mat(Dv);
}

void check_forward_pass(struct parameters * p)
{
	p->ngram = 4;
	p->dict_size = 5;
	p->dim = 2;
	struct model * m = init_model(p);

	m->D[0]->value[0] = 1.0;
	m->D[0]->value[1] = 2.0;
	m->D[0]->value[2] = 3.0;
	m->D[0]->value[3] = 4.0;
	
	m->D[1]->value[0] = 4.0;
	m->D[1]->value[1] = 3.0;
	m->D[1]->value[2] = 2.0;
	m->D[1]->value[3] = 1.0;

	m->D[2]->value[0] = 1.0;
	m->D[2]->value[1] = 3.0;
	m->D[2]->value[2] = 5.0;
	m->D[2]->value[3] = 7.0;

	m->M->value[0] = 1.0;
	m->M->value[1] = 2.0;
	m->M->value[2] = 3.0;
	m->M->value[3] = 4.0;
	m->M->value[4] = 5.0;
	m->M->value[5] = 6.0;
	m->M->value[6] = 7.0;
	m->M->value[7] = 8.0;
	m->M->value[8] = 9.0;
	m->M->value[9] = 10.0;

	Mat * y = init_mat(p->dict_size,1,ZEROS);
	int * wid = (int *)malloc(p->ngram * sizeof(int));

	wid[0] = 0;
	wid[1] = 1;
	wid[2] = 2;
	wid[3] = 3;
	
	fake_forward(m,p,y,wid);
	int i;
	for(i=0;i<p->dict_size;i++)
		printf("%f\n",y->value[i]);

	free(wid);
	free_mat(y);
	exit(0);
}

double cross_entropy(Mat * y, int t)
{
	return -log(y->value[t]);
}

void check_backward_pass(struct parameters * p)
{
	p->ngram = 5;
	p->dict_size = 5;
	p->dim = 2;
	struct model * m = init_model(p);
	struct grad * g = init_grad(p);
	Mat * y = init_mat(p->dict_size,1,ZEROS);
	Mat * y1 = init_mat(p->dict_size,1,ZEROS);
	Mat * y2 = init_mat(p->dict_size,1,ZEROS);

	m->D[0]->value[0] = 0.1;
	m->D[0]->value[1] = 0.2;
	m->D[0]->value[2] = 0.3;
	m->D[0]->value[3] = 0.4;
	
	m->D[1]->value[0] = 0.1;
	m->D[1]->value[1] = 0.2;
	m->D[1]->value[2] = 0.3;
	m->D[1]->value[3] = 0.4;

	m->D[2]->value[0] = 0.1;
	m->D[2]->value[1] = 0.2;
	m->D[2]->value[2] = 0.3;
	m->D[2]->value[3] = 0.4;

	m->D[3]->value[0] = 0.1;
	m->D[3]->value[1] = 0.2;
	m->D[3]->value[2] = 0.3;
	m->D[3]->value[3] = 0.4;
	
	m->M->value[0] = 0.1;
	m->M->value[1] = 0.2;
	m->M->value[2] = 0.3;
	m->M->value[3] = 0.4;
	m->M->value[4] = 0.5;
	m->M->value[5] = 0.6;
	m->M->value[6] = 0.7;
	m->M->value[7] = 0.8;
	m->M->value[8] = 0.9;
	m->M->value[9] = 1.0;

	m->b->value[0] = 0.5;
	m->b->value[1] = 0.4;
	m->b->value[2] = 0.3;
	m->b->value[3] = 0.2;
	m->b->value[4] = 0.1;

	int * wid = (int *)malloc(p->ngram * sizeof(int));
	int i;
	for(i=0;i<p->ngram;i++) {wid[i]=i;}
	fake_forward(m,p,y,wid);
	fake_backward(m,p,y,wid,g);
	
	double eps = 0.00001;
	int ind = 0;
	double err1,err2,num_grad,net_grad,diff;
	/*
	printf("y: ");
	for(i=0;i<p->dict_size;i++)
		printf("%f,",y->value[i]);
	printf("\n");
	
	printf("dEdb: ");
	for(i=0;i<p->dict_size;i++)
		printf("%f,",g->dEdb->value[i]);
	printf("\n");

	printf("dEdD: ");
	for(i=0;i<p->dim*p->dim;i++)
		printf("%f,",g->dEdD[0]->value[i]);
	printf("\n");
	
	printf("dEdM: ");
	for(i=0;i<p->dim*p->dict_size;i++)
		printf("%f,",g->dEdM->value[i]);
	printf("\n");
	*/
	
	

	
	// check g->b
	m->b->value[ind] += eps;
	fake_forward(m,p,y1,wid);
	err1 = cross_entropy(y1,wid[p->ngram-1]);
	m->b->value[ind] -= 2*eps;
	fake_forward(m,p,y2,wid);
	err2 = cross_entropy(y2,wid[p->ngram-1]);
	num_grad = (err1-err2)/2/eps;
	net_grad = g->dEdb->value[ind];
	diff = fabs(num_grad-net_grad);
	m->b->value[ind] += eps;
	printf("check b\n");
	printf("%f,%f,%f,%f\n",num_grad,net_grad,diff,m->b->value[ind]);
	// check g->D[i]
	int n = 1;
	m->D[n]->value[ind] += eps;
	fake_forward(m,p,y1,wid);	

	err1 = cross_entropy(y1,wid[p->ngram-1]);
	m->D[n]->value[ind] -= 2*eps;
	fake_forward(m,p,y2,wid);
	err2 = cross_entropy(y2,wid[p->ngram-1]);
	num_grad = (err1-err2)/2/eps;
	net_grad = g->dEdD[n]->value[ind];
	diff = fabs(num_grad-net_grad);
	m->D[n]->value[ind] += eps;
	printf("check D\n");
	printf("%f,%f,%f,%f\n",num_grad,net_grad,diff,m->D[n]->value[ind]);
	
	// check g->M
	
	m->M->value[ind] += eps;
	fake_forward(m,p,y1,wid);
	
	err1 = cross_entropy(y1,wid[p->ngram-1]);
	m->M->value[ind] -= 2*eps;
	fake_forward(m,p,y2,wid);
	err2 = cross_entropy(y2,wid[p->ngram-1]);

	num_grad = (err1-err2)/2/eps;
	net_grad = g->dEdM->value[ind];
	diff = fabs(num_grad-net_grad);
	printf("check M\n");
	printf("%f,%f,%f,%f\n",num_grad,net_grad,diff,m->M->value[ind]);
	

	free(wid);
	free_mat(y);
	free_grad(g,p);
	free_mat(y1);
	free_mat(y2);
	exit(0);
}

void check_norm(struct parameters * p)
{
	p->dim = 2;
	p->dict_size = 3;
	p->ngram = 5;	
	struct model * m = init_model(p);
	m->M->value[0] = 1;
	m->M->value[1] = 2;
	m->M->value[2] = 3;
	m->M->value[3] = 4;
	m->M->value[4] = 5;
	m->M->value[5] = 6;		
	norm_cons(m,p);
	int i;
	for(i=0;i<6;i++) printf("%f\n",m->M->value[i]);

	free(m);
}

void check_sem_constr(struct parameters * p)
{
	p->ngram = 5;
	p->dict_size = 5;
	p->dim = 2;
	struct model * m = init_model(p);
	struct grad * g = init_grad(p);
	Mat * y = init_mat(p->dict_size,1,ZEROS);
	Mat * y1 = init_mat(p->dict_size,1,ZEROS);
	Mat * y2 = init_mat(p->dict_size,1,ZEROS);

	m->D[0]->value[0] = 0.1;
	m->D[0]->value[1] = 0.2;
	m->D[0]->value[2] = 0.3;
	m->D[0]->value[3] = 0.4;
	
	m->D[1]->value[0] = 0.1;
	m->D[1]->value[1] = 0.2;
	m->D[1]->value[2] = 0.3;
	m->D[1]->value[3] = 0.4;

	m->D[2]->value[0] = 0.1;
	m->D[2]->value[1] = 0.2;
	m->D[2]->value[2] = 0.3;
	m->D[2]->value[3] = 0.4;

	m->D[3]->value[0] = 0.1;
	m->D[3]->value[1] = 0.2;
	m->D[3]->value[2] = 0.3;
	m->D[3]->value[3] = 0.4;
	
	m->M->value[0] = 0.1;
	m->M->value[1] = 0.2;
	m->M->value[2] = 0.3;
	m->M->value[3] = 0.4;
	m->M->value[4] = 0.5;
	m->M->value[5] = 0.6;
	m->M->value[6] = 0.7;
	m->M->value[7] = 0.8;
	m->M->value[8] = 0.9;
	m->M->value[9] = 1.0;

	m->b->value[0] = 0.5;
	m->b->value[1] = 0.4;
	m->b->value[2] = 0.3;
	m->b->value[3] = 0.2;
	m->b->value[4] = 0.1;

	

}
