#include <cblas.h>
#include <stdio.h>
#include <string.h>

typedef struct constraint {
	char * name;
	int num;
	int ** ques;
}constr;

void norm_cons(struct model * m, struct parameters * p)
{
	// return;
	double norm;
	int i;
	for(i=0;i<p->dict_size;i++)
	{	
		norm = cblas_dnrm2(p->dim,m->M->value+i*p->dim,1);
		cblas_dscal(p->dim,1.0/norm,m->M->value+i*p->dim,1);
	}
}

void sem_cons(struct model * m, struct parameters * p, struct grad * g, constr ** c)
{
	int i,j,k,max_ind=0; double max_val;

	Mat * u = init_mat(p->dim,1,ZEROS);
	Mat * v = init_mat(p->dim,1,ZEROS);
	Mat * w = init_mat(p->dict_size,1,ZEROS);

	Mat * score = init_mat(p->dict_size,1,ZEROS);

	for(i=0;i<p->categories;i++)
	{
		// v1-v2+v3 == v4?
		for(j=0;j<c[i]->num;j++)
		{
			// load u = v1, v = v2
			col_select(u, m->M, c[i]->ques[j][0]);
			col_select(v, m->M, c[i]->ques[j][1]);
			// u = v1-v2
			elem_op(u, 1.0, u, 1.0, v, MINUS);
			// load v = v3
			col_select(v, m->M, c[i]->ques[j][2]);
			// u = v1-v2+v3
			elem_op(u, 1.0, u, 1.0, v, PLUS);
			
			mat_mult(score, 1.0, m->M, trans, u, notrans, 0.0);
			
			max_val = score->value[0];
			for(k=1;k<p->dict_size;k++)
			{
				if(k==c[i]->ques[j][3]) continue;
				if(score->value[k] > max_val) {
					max_val = score->value[k];
					max_ind = k;
				}
			}
			
			if(score->value[c[i]->ques[j][3]] < max_val + p->margin)
			{
				// grad: M(-w_0*w_t^T-w_t*w_0^T+w_0*w_j^T+w_j*w_0^T)
				// = M(w_0*(w_j-w_t)^T+(w_j-w_t)*w_0^T)
				// = u*(w_j-w_t)^T+M*(w_j-w_t)*w_0^T

				// u = v1-v2+v3
				// w = w_j-w_t
				w->value[c[i]->ques[j][3]] = -1.0;
				w->value[max_ind] = 1.0;
		
				// g->dEdM = u*w^T				
				mat_mult(g->dEdM,1.0,u,notrans,w,trans,0.0);
				
				// v = M*(w_j-w_t)
				mat_mult(v,1.0,m->M,notrans,w,notrans,p->lambda_sem);
				
				// w = w_0
				w->value[c[i]->ques[j][3]] = 0.0;
				w->value[max_ind] = 0.0;
				w->value[c[i]->ques[j][0]] = 1.0;
				w->value[c[i]->ques[j][1]] = -1.0;
				w->value[c[i]->ques[j][2]] = 1.0;

				// g->dEdM += M*(w_j-w_t)*w_0^T = v*w_0^T
				mat_mult(g->dEdM,p->lambda_sem,v,notrans,w,trans,p->lambda_sem);
			}															
		}
	}

}

void parse(constr * c, int * buffer, int cnt)
{
	cnt /= 2;
	c->num = cnt * (cnt-1)/2;
	if(c->num == 0) return;
	c->ques = (int **)malloc(c->num * sizeof(int *));
	int i;
	int m=0,n=1;

	for(i=0;i<c->num;i++)
	{
		c->ques[i] = (int *)malloc(4 * sizeof(int));
		c->ques[i][0] = buffer[2*m];
		c->ques[i][1] = buffer[2*m+1];
		c->ques[i][2] = buffer[2*n];
		c->ques[i][3] = buffer[2*n+1];
		if(n == cnt-1) {m++; n = m+1;}
		else n++;
	}
}

void load_cons(constr ** train_constr,constr ** valid_constr,constr ** test_constr,struct parameters * p)
{
	FILE * fp = fopen(p->constraints,"r");
	char * line = NULL;
	char *saveptr;
        size_t len = 0;
	char * pch;
	int * buffer = (int *)malloc(1000*sizeof(int));
	int cnt = 0;
	int cate_cnt = 0; // category count
	int i;
	constr ** cons_ptr;
	constr ** ptr;

	for(i=0;i<p->categories;i++) train_constr[i] = (constr *)malloc(sizeof(constr));
	for(i=0;i<p->categories;i++) valid_constr[i] = (constr *)malloc(sizeof(constr));
	for(i=0;i<p->categories;i++) test_constr[i] = (constr *)malloc(sizeof(constr));
	
	while ((getline(&line, &len, fp)) != -1) {
		
		if(strstr(line,"training:") != NULL)
		{
			cons_ptr = train_constr;
			ptr = cons_ptr;	
		}
		else if(strstr(line,"valid:") != NULL)
		{
			cons_ptr = valid_constr;
		}
		else if(strstr(line,"testing:") != NULL)
		{
			cons_ptr = test_constr;
		}
		else if(line[0] == ':')
		{
			if(cnt != 0) 
			 	parse(ptr[cate_cnt-1], buffer, cnt);
			
			if(cate_cnt == p->categories) {cate_cnt = 0; ptr = cons_ptr;}

			cnt = 0;
			cons_ptr[cate_cnt]->name = (char *)malloc(100*sizeof(char));
			* (line+strlen(line)-1) = '\0';
			strcpy(cons_ptr[cate_cnt]->name,line);
			cate_cnt++;
		}
		else
		{
			pch = strtok_r (line,",",&saveptr);
			buffer[cnt] = atoi(pch);
			cnt++;
			pch = strtok_r (NULL,",",&saveptr);
			buffer[cnt] = atoi(pch);
			cnt++;			
		}
	}
	parse(ptr[cate_cnt-1], buffer, cnt);
	
	fclose(fp);
	free(buffer);
	if(line) free(line);	
}

void test_cons(constr ** c, struct model * m, struct parameters * p, char * mode)
{
	int i,j,k,max_ind=0;
	double max_val,curr_cnt = 0.0, all_cnt = 0.0, curr_ques = 0.0, all_ques = 0.0;

	Mat * u = init_mat(p->dim,1,ZEROS);
	Mat * v = init_mat(p->dim,1,ZEROS);

	Mat * score = init_mat(p->dict_size,1,ZEROS);

	for(i=0;i<p->categories;i++)
	{
		curr_cnt = 0; all_cnt = 0;
		// v1-v2+v3 == v4?
		for(j=0;j<c[i]->num;j++)
		{
			// load u = v1, v = v2
			col_select(u, m->M, c[i]->ques[j][0]);
			col_select(v, m->M, c[i]->ques[j][1]);
			// u = v1-v2
			elem_op(u, 1.0, u, 1.0, v, MINUS);
			// load v = v3
			col_select(v, m->M, c[i]->ques[j][2]);
			// u = v1-v2+v3
			elem_op(u, 1.0, u, 1.0, v, PLUS);
		
			mat_mult(score, 1.0, m->M, trans, u, notrans, 0.0);

			max_val = score->value[0];
			for(k=0;k<p->dict_size;k++)
			{
				if(score->value[k] > max_val) {
					max_val = score->value[k];
					max_ind = k;
				}
			}

			printf("%d,%d\n",max_ind,c[i]->ques[j][3]);
			if(max_ind == c[i]->ques[j][3]) {curr_cnt++; all_cnt++;}
			curr_ques++; all_ques++;					
		}
		printf("%s\n",c[i]->name);
		printf("Accuracy: %f\n",curr_cnt/curr_ques);

		if(strcmp(mode,"train") == 0)	m->train_accu = all_cnt/all_ques;
		else if (strcmp(mode,"valid")==0)	m->valid_accu = all_cnt/all_ques;
		else if (strcmp(mode,"test")==0)	m->test_accu = all_cnt/all_ques;
	}
}


void free_constr(constr ** c, struct parameters * p)
{
	int i,j;
	
	for(i=0;i<p->categories;i++)
	{				
		free(c[i]->name);	
		
		if(c[i]->num != 0) 	
			for(j=0;j<c[j]->num;j++)
			{
				free(c[i]->ques[j]);
			}	 
		free(c[i]->ques);
	}

	free(c);
}
