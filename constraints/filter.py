import sys

def filt(fname):
	with open('pairs.txt') as f:
		pairs = f.readlines()
	with open(fname) as f:
		wordlist = f.readlines()
		# must be lower case
	train_pairs = []
	valid_pairs = []
	test_pairs = []
	tmp = []	

	for i in xrange(len(pairs)):
		if pairs[i][0] == ':':
			train_pairs.append(pairs[i].rstrip())
			test_pairs.append(pairs[i].rstrip())
			valid_pairs.append(pairs[i].rstrip())
			tmp = [] # all pairs under a catagory
		else:
			p = pairs[i].rstrip().split(' ')
			
			if p[0].lower()+'\n' in wordlist and p[1].lower()+'\n' in wordlist:
				tmp.append(p)

			if i == len(pairs)-1:
				if len(tmp) >= 4:
					for j in xrange(len(tmp)):
						t = [wordlist.index(w.lower()+'\n') for w in tmp[j]]
						if j%4 < 2:
							train_pairs.append(t)
						elif j%4 == 2:
							valid_pairs.append(t)
						elif j%4 == 3:
							test_pairs.append(t)


			elif pairs[i+1][0] == ':': 
				if len(tmp) >= 4:
					for j in xrange(len(tmp)):
						t = [wordlist.index(w.lower()+'\n') for w in tmp[j]]
						if j%4 < 2:
							train_pairs.append(t)
						elif j%4 == 2:
							valid_pairs.append(t)
						elif j%4 == 3:
							test_pairs.append(t)

	with open('filtered_pairs.txt','w+') as f:
		f.write('training:\n')
		for p in train_pairs:
			if p[0] == ':':
				f.write(p+'\n')
			else:
				f.write(','.join(str(t) for t in p)+'\n')
		f.write('validation:\n')
		for p in valid_pairs:
			if p[0] == ':':
				f.write(p+'\n')
			else:
				f.write(','.join(str(t) for t in p)+'\n')
		f.write('testing:\n')
		for p in test_pairs:
			if p[0] == ':':
				f.write(p+'\n')
			else:
				f.write(','.join(str(t) for t in p)+'\n')
						


if __name__=='__main__':
	# word list
	fname = sys.argv[1]
	filt(fname)
	
