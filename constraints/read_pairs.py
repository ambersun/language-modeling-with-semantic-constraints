with open('questions-words.txt') as f:
	lines = f.readlines()

pairs = []

with open('pairs.txt','w+') as f:
	for i in xrange(len(lines)):
		if lines[i][0] == ':':
			f.write(lines[i])
			pairs = []
				
		else:
			toks = lines[i].rstrip().split(' ')
			p1 = toks[0],toks[1]
			p2 = toks[2],toks[3]
 			if p1 not in pairs:
				pairs.append(p1)
			if p2 not in pairs:
				pairs.append(p2)
			if i == len(lines)-1:
				for p in pairs:
					f.write(p[0]+' '+p[1]+'\n')
			elif lines[i+1][0] == ':':
				for p in pairs:
					f.write(p[0]+' '+p[1]+'\n')

			
			
